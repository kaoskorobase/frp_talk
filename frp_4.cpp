#include "frp.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

using boost::make_optional;
using boost::none;
using boost::optional;

// Play sample only on note-on

stream<Note> notes(stream<Message> messages)
{
    return filter_optional(messages.map([](Message msg) -> optional<Note> {
        switch (msg.status) {
        case kNoteOn:
          return Note(Note::On, get<0>(msg.data), get<1>(msg.data) / 127.0);
        case kNoteOff:
          return Note(Note::Off, get<0>(msg.data), get<1>(msg.data) / 127.0);
        default:
          return none;
        }
    }));
}

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    stream<Synth> synths = filter_optional(notes(input).map([](Note note) {
        return note.state == Note::On
            ? make_optional(Synth("reverbkick"))
            : none;
    }));

    stream<string> messages = input.map([](Message msg) {
        stringstream s;
        s << msg;
        return s.str();
    });

    return make_tuple(synths, messages);
}

// -> Filter note-ons
// -> Add velocity
