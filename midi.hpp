#ifndef MIDI_HPP_INCLUDED
#define MIDI_HPP_INCLUDED

#include <cstdint>
#include <ostream>
#include <tuple>

namespace MIDI
{
    constexpr int kNoteOn = 0x09;
    constexpr int kNoteOff = 0x08;
    constexpr int kCtrlChange = 0x0B;

    struct Message
    {
        int status;
        std::tuple<int,int> data;
    };

    inline std::ostream& operator<<(std::ostream& out, const Message& x)
    {
        out << "Message(" << x.status << ", " << std::get<0>(x.data) << ", " << std::get<1>(x.data) << ")";
        return out;
    }

    struct Note
    {
        enum State { On, Off };

        Note(State state, int note, float velocity)
            : state(state), note(note), velocity(velocity)
        {}

        State state;
        int note;
        float velocity;
    };

    inline std::ostream& operator<<(std::ostream& out, const Note& x)
    {
        out << "NoteOn(" << x.state << ", " << x.note << ", " << x.velocity << ")";
        return out;
    }

    struct Ctrl
    {
        Ctrl(int controller, float value)
            : controller(controller), value(value)
        {}

        int controller;
        float value;
    };

    inline std::ostream& operator<<(std::ostream& out, const Ctrl& x)
    {
        out << "Ctrl(" << x.controller << ", " << x.value << ")";
        return out;
    }
}

#endif // MIDI_HPP_INCLUDED

