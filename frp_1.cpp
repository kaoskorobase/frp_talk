#include "frp.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    stream<Synth> synths;
    stream<string> messages;
    return make_tuple(synths, messages);
}

// -> Print incoming messages
