# Stop listening! An introduction to functional reactive programming (FRP)

> FRP models stateful event-based logic without listeners.

## What is functional reactive programming

* Replacement for observer pattern (listeners, callbacks)
* Composable, modular way to code event-driven logic
* Expresses programs as reaction to their inputs
* Brings order to the management of program state
* Something fundamental: Anyone who tries to solve the problems in the observer pattern will eventually invent FRP
* Implemented as a lightweight software library in a standard programming language (Sodium)
* Complete embedded language for stateful logic
* Leads to cleaner, clearer, more robust, more maintainable code

## Definition

> There are a lot of ideas floating around about what FRP is. For me, it's
> always been two things: (a) denotative and (b) temporally continuous. Many
> folks drop both of these properties and identify FRP with various
> _implementation_ notions, all of which are beside the point in my
> perspective.
>
> By "denotative", I mean founded on a precise, simple,
> implementation-independent, compositional semantics that exactly specifies
> the meaning of each type and building block. The compositional nature of the
> semantics then determines the meaning of all type-correct combinations of the
> building blocks.

(Conal Elliot, author of "Push-pull functional reactive programming")

## Six problems with listeners

* **Unpredictable order** - In a complex network of listeners, the order in which events are received can depend on the order in which you registered the listeners.
  
* **Missed first event** - It can be difficult to guarantee that you've registered your listeners before you send the first event.

* **Messy state** - Callbacks push your code into a state-machine style, which gets messy fast.

* **Threading issues** - Attempting to make listeners thread-safe can lead to dead-locks, and it can be difficult to guarantee that no more callbacks will be received after deregistering a listener.

* **Leaking callbacks** - If you forget to deregister your listener, your program will leak memory. Listeners reverse the natural data dependency but don't reverse the keep-alive dependency.

* **Accidental recursion** - The order in which you update local state and notify listeners can be crucial, and it's easy to make mistakes.

FRP solves all of these problems.

## Basic concepts

FRP uses two fundamental data types

* *Cells* represent values that *change over time*
* *Streams* represent *streams of events*

FRP provides a minimal embedded language for writing expressions with these types, i.e. functions that combine streams and cells and return new streams and cells.

Internally, at initialization time, FRP creates a directed graph of listeners that are invoked as events enter the system at runtime.

> Stages of execution of an FRP program (p. 13, Figure 1.6)

## Benefits

* Thinking in terms of dependency rather than sequences of steps
* Thinking declaratively: What instead of how

## Requirements

The functions you pass to the FRP library must only stick to *one* requirement: **Referential transparency**.

> All functions passed to FRP primitives must be referentially
> transparent, or pure. This means that they must not perform I/O, modify or
> read external state, or keep internal state. Cheating always ends in tears.
> This is important.

## Links

* This talk: [bit.ly/2y4dgso](https://bitbucket.org/kaoskorobase/frp_talk/)
* [Function Reactive Programming](https://www.manning.com/books/functional-reactive-programming) book by Stephen Blackheath and Anthony Jones
* [Sodium](http://sodium.nz/) forum
* [Sodium for C++](https://github.com/SodiumFRP/sodium-cxx)

## Running the examples

Before running the examples, please

    $ git update --init --recursive

For compiling you need cmake, boost and portmidi (available via homebrew).

For directly running examples 1-9 you can

    $ ./frp 1

but you will probably need to adapt the script to use the correct MIDI devices.

