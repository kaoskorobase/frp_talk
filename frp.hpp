#ifndef FRP_HPP_INCLUDED
#define FRP_HPP_INCLUDED

#include "midi.hpp"

#include <boost/optional.hpp>
#include <sodium/sodium.h>
#include <string>
#include <tuple>
#include <vector>

namespace FRP
{
    class Synth
    {
    public:
        Synth(const std::string& sound, float amp=1.0f, float speed=1.0f, float pan=0.5f)
            : m_sound(sound)
            , m_amp(amp)
            , m_speed(speed)
            , m_pan(pan)
        {}

        const std::string& sound() const
        {
            return m_sound;
        }

        Synth sound(const std::string& value) const
        {
            Synth result(*this);
            result.m_sound = value;
            return result;
        }

        float amp() const
        {
            return m_amp;
        }

        Synth amp(float value) const
        {
            Synth result(*this);
            result.m_amp = value;
            return result;
        }

        float speed() const
        {
            return m_speed;
        }

        Synth speed(float value) const
        {
            Synth result(*this);
            result.m_speed = value;
            return result;
        }

        float pan() const
        {
            return m_pan;
        }

        Synth pan(float value) const
        {
            Synth result(*this);
            result.m_pan = value;
            return result;
        }

    private:
        std::string m_sound;
        float m_amp;
        float m_speed;
        float m_pan;

    };

    std::tuple<sodium::stream<Synth>,
               sodium::stream<std::string>> network(sodium::stream<MIDI::Message> input);
}

#endif // FRP_HPP_INCLUDED
