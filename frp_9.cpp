#include "frp.hpp"
#include "util.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

using boost::make_optional;
using boost::none;
using boost::optional;

// Persistent pan

class NanoPad
{
    stream<Ctrl> m_controls;

public:
    stream<Note> pads;
    stream<bool> xyActivate;
    cell<bool> xyActive;
    stream<float> x;
    stream<float> y;

    NanoPad(stream<Message> messages)
        : m_controls(
            filter_optional(messages.map([](Message msg) -> optional<Ctrl> {
                switch(msg.status) {
                case kCtrlChange:
                    return Ctrl(get<0>(msg.data), get<1>(msg.data) / 127.0);
                default:
                    return none;
                }
            })))
        , xyActivate(
            m_controls.filter([](const Ctrl& ctrl) { return ctrl.controller == 16; })
                      .map([](const Ctrl& ctrl) { return ctrl.value > 0; }))
        , xyActive(xyActivate.hold(false))
    {
        pads = filter_optional(messages.map([](Message msg) -> optional<Note> {
            switch (msg.status) {
            case kNoteOn:
              return Note(Note::On, get<0>(msg.data), get<1>(msg.data) / 127.0);
            case kNoteOff:
              return Note(Note::Off, get<0>(msg.data), get<1>(msg.data) / 127.0);
            default:
              return none;
            }
        }));
        x = m_controls
            .filter([](const Ctrl& ctrl) { return ctrl.controller == 1; })
            .map([](const Ctrl& ctrl) { return ctrl.value; });
        y = m_controls
            .filter([](const Ctrl& ctrl) { return ctrl.controller == 2; })
            .map([](const Ctrl& ctrl) { return ctrl.value; });
    }
};

optional<int> samplePad(const Note& note)
{
    return note.note >= 44 && note.note <= 51
        ? make_optional(note.note - 44)
        : none;
}

// Cheap abstraction!
//
// TODO: Make it a template.
cell<float> deltaTouch(stream<unit> reset, stream<float> value)
{
    cell_loop<tuple<optional<float>,float>> result;

    stream<tuple<optional<float>,float>> update =
        value.snapshot(result, [](float x, tuple<optional<float>,float> s) {
            if (get<0>(s)) {
                return make_tuple(get<0>(s), x - *get<0>(s));
            } else {
                return make_tuple(make_optional(x), 0.f);
            }
        });

    result.loop(reset.map([](unit) { return make_tuple<optional<float>,float>(none, 0.f); })
                .or_else(update)
                .hold(make_tuple<optional<float>,float>(none, 0.f)));

    return result.map([](tuple<optional<float>,float> x) { return get<1>(x); });
}

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    const NanoPad nanoPad(input);

    static const auto sampleMap  = FRP::readFileLines("samples1.txt");
    static const auto sampleMap2 = FRP::readFileLines("samples3.txt");

    stream<unit> shiftE(
        nanoPad.pads.filter([](Note note) {
            return note.note == 36 && note.state == Note::On;
        })
        .map([](Note note) { return unit(); })
    );

    cell_loop<bool> shift;

    shift.loop(shiftE.snapshot(shift, [](unit, bool value) { return !value; }).hold(false));

    cell<vector<string>> samples(shift.map([&](bool x) {
        return x ? sampleMap2 : sampleMap;
    }));

    stream<Synth> synth =
        filter_optional(
            nanoPad.pads
            .map([](Note note) -> optional<tuple<int,float>> {
                const auto pad = samplePad(note);
                if (pad && note.state == Note::On) {
                    return make_optional(make_tuple(*pad, note.velocity));
                } else {
                    return none;
                }
            })
        )
        .snapshot(samples, [](tuple<int,float> pad, vector<string> sampleMap) {
            return Synth(sampleMap.at(get<0>(pad)), get<1>(pad));
        });

    cell<float> speed = nanoPad.xyActivate
                           .filter([](bool x) { return !x; })
                           .map([](bool) { return 1.f; })
                           .or_else(nanoPad.y.map([](float x) { return x * 1.5f + 0.5f; }))
                           .hold(1.f);

    // Deltas when touched and moved, like a touch pad.
    cell<float> touchX = deltaTouch(nanoPad.xyActivate.map([](bool) { return unit(); }), nanoPad.x);

    // Pan accumulates deltas, starting at 0.5 (in the middle)
    cell_loop<float> pan;
    pan.loop(
        nanoPad.x.snapshot(pan, touchX, [](float x, float pan, float dx) {
            return min(max(0.f, pan + .05f * dx), 1.f);
        }).hold(.5f)
    );

    // Apply pan to synth params
    stream<Synth> synthWithParams = synth.snapshot(speed, pan, ([](const Synth& x, float speed, float pan) {
        return x.speed(speed).pan(pan);
    }));

    stream<string> messages = nanoPad.x.snapshot(pan, [](float x, float pan) {
        stringstream s;
        s << pan;
        return s.str();
    });

    return make_tuple(synthWithParams, messages);
}

