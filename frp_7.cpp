#include "frp.hpp"
#include "util.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

using boost::make_optional;
using boost::none;
using boost::optional;

// Alternative sample map

stream<Note> notes(stream<Message> messages)
{
    return filter_optional(messages.map([](Message msg) -> optional<Note> {
        switch (msg.status) {
        case kNoteOn:
          return Note(Note::On, get<0>(msg.data), get<1>(msg.data) / 127.0);
        case kNoteOff:
          return Note(Note::Off, get<0>(msg.data), get<1>(msg.data) / 127.0);
        default:
          return none;
        }
    }));
}

optional<int> samplePad(const Note& note)
{
    return note.note >= 44 && note.note <= 51
        ? make_optional(note.note - 44)
        : none;
}

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    static const auto sampleMap  = FRP::readFileLines("samples1.txt");
    static const auto sampleMap2 = FRP::readFileLines("samples3.txt");

    stream<Note> pads = notes(input);

    stream<unit> shiftE(
        pads.filter([](Note note) {
            return note.note == 36 && note.state == Note::On;
        })
        .map([](Note note) { return unit(); })
    );

    cell_loop<bool> shift;

    shift.loop(shiftE.snapshot(shift, [](unit, bool value) { return !value; }).hold(false));

    cell<vector<string>> samples(shift.map([&](bool x) {
        return x ? sampleMap2 : sampleMap;
    }));

    stream<Synth> synths =
        filter_optional(
            notes(input)
            .map([](Note note) -> optional<tuple<int,float>> {
                const auto pad = samplePad(note);
                return pad && note.state == Note::On
                    ? make_optional(make_tuple(*pad, note.velocity))
                    : none;
            })
        )
        .snapshot(samples, [](tuple<int,float> pad, vector<string> sampleMap) -> Synth {
            return Synth(sampleMap.at(get<0>(pad)), get<1>(pad));
        });

    stream<string> messages = input.map([](Message msg) {
        stringstream s;
        s << msg;
        return s.str();
    });

    return make_tuple(synths, messages);
}

// -> Modify playback speed
