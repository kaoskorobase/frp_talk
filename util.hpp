#ifndef FRP_TALK_UTIL_HPP
#define FRP_TALK_UTIL_HPP

#include <fstream>
#include <string>
#include <vector>

namespace FRP
{
    inline std::vector<std::string> readFileLines(const std::string& path)
    {
        std::ifstream file(path);
        std::string line;
        std::vector<std::string> result;
        while (std::getline(file, line))
        {
            result.push_back(line);
        }
        return result;
    }
}

#endif // FRP_TALK_UTIL_HPP

