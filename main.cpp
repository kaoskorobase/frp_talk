#include "frp.hpp"
#include "midi.hpp"

#include <oscpp/server.hpp>
#include <oscpp/print.hpp>
#include <portmidi.h>
#include <sodium/sodium.h>

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <array>
#include <chrono>
#include <iostream>

#include <unistd.h> // usleep

const double kNTPr2i = uint64_t(1)<<32;
const double kNTPi2r = 1.0 / kNTPr2i;
const long kMIDILatency = 100;
const size_t kEventBufferSize = 8;

uint64_t getNTPTime()
{
  using namespace std::chrono;
  const uint64_t ntp_ut_epoch_diff = (70ull * 365ull + 17ull) * 24ull * 60ull * 60ull;
  const auto t = duration_cast<duration<double>>(system_clock().now().time_since_epoch());
  return (uint64_t)(t.count() * kNTPr2i + .5) + (ntp_ut_epoch_diff << 32);
}

long ntp_dur_to_ms(uint64_t ntp)
{
  const double ntp_to_ms = kNTPi2r * 1000.0;
  return (long)(ntp * ntp_to_ms + .5);
}

void check(PmError err)
{
  if (err != pmNoError) {
    std::stringstream s;
    s << "MIDI error: " << Pm_GetErrorText(err);
    throw std::runtime_error(s.str());
  }
}

int check(int result)
{
    if (result < 0) {
        check(result);
    }
    return result;
}

int32_t mkChannelMessage(int8_t msgType, OSCPP::Server::ArgStream& args)
{
  const int8_t channel = args.int32() & 0xF;
  const int8_t data0 = args.int32() & 0xFF;
  const int8_t data1 = args.int32() & 0xFF;
  const uint8_t status = (msgType & 0xF0) | channel;
  return Pm_Message(status, data0, data1);
}

void send(std::shared_ptr<PortMidiStream> stream, long ms, int8_t msgType, OSCPP::Server::ArgStream& args)
{
  check(Pm_WriteShort(stream.get(), ms, mkChannelMessage(msgType, args)));
}

void handlePacket(uint64_t startTime, uint64_t packetTime, const OSCPP::Server::Packet& packet, std::shared_ptr<PortMidiStream> stream)
{
  try {
    if (packet.isBundle()) {
        // Convert to bundle
        OSCPP::Server::Bundle bundle(packet);

        // Get packet stream
        OSCPP::Server::PacketStream packets(bundle.packets());

        // Iterate over all the packets and call handlePacket recursively.
        // Cuidado: Might lead to stack overflow!
        const uint64_t bundleTime = bundle.time();
        while (!packets.atEnd()) {
            handlePacket(startTime, bundleTime, packets.next(), stream);
        }
    } else {
      // Convert to message
      OSCPP::Server::Message msg(packet);
      std::cerr << msg << std::endl;
      try {
        // Get argument stream
        OSCPP::Server::ArgStream args(msg.args());
        const long ms = packetTime < startTime ? 0 : ntp_dur_to_ms(packetTime - startTime);
        if (ms == 0) {
          std::cerr << "Packet too late by " << ntp_dur_to_ms(startTime - packetTime) << "ms" << std::endl;
        }
        if (msg == "/midi/note_on") {
          send(stream, ms, 0x90, args);
        } else if (msg == "/midi/note_off") {
          send(stream, ms, 0x10, args);
        } else if (msg == "/midi/cc") {
          send(stream, ms, 0xB0, args);
        } else {
            std::cerr << "Unknown message: " << msg << std::endl;
        }
      } catch (OSCPP::Error& e) {
        std::cerr << "OSC error in message " << msg.address() << ": " << e.what() << std::endl;
      } catch (std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
      }
    }
  } catch (OSCPP::Error& e) {
    std::cerr << "OSC error in bundle: " << e.what() << std::endl;
  }
}

class PortMidi
{
  static void terminate()
  {
    // std::cout << "PortMidi::terminate()\n";
    Pm_Terminate();
  }

  static void handler(
      const boost::system::error_code& error,
      int signal_number)
  {
    terminate();
    if (!error)
    {
      // A signal occurred.
    }
  }

public:
  PortMidi(boost::asio::io_service& io_service)
  {
    Pm_Initialize();
    // Construct a signal set registered for process termination.
    boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
    // Start an asynchronous wait for one of the signals to occur.
    signals.async_wait(handler);
  }

  ~PortMidi()
  {
    terminate();
  }

  std::vector<std::tuple<PmDeviceID,std::string>> inputDevices() const
  {
    std::vector<std::tuple<PmDeviceID,std::string>> result;
    const int numDevices = Pm_CountDevices();
    if (numDevices > 0) {
      result.reserve(numDevices);
      for (int i=0; i < numDevices; i++) {
        const PmDeviceInfo* info = Pm_GetDeviceInfo(i);
        if (info->input) {
          result.push_back(std::make_tuple(i, info->name));
        }
      }
    }
    return result;
  }

  std::vector<std::tuple<PmDeviceID,std::string>> outputDevices() const
  {
    std::vector<std::tuple<PmDeviceID,std::string>> result;
    const int numDevices = Pm_CountDevices();
    if (numDevices > 0) {
      result.reserve(numDevices);
      for (int i=0; i < numDevices; i++) {
        const PmDeviceInfo* info = Pm_GetDeviceInfo(i);
        if (info->output) {
          result.push_back(std::make_tuple(i, info->name));
        }
      }
    }
    return result;
  }
};

static void handler(
    const boost::system::error_code& error,
    int signal_number)
{
  std::cerr << "signal " << signal_number << "\n";
  if (!error)
  {
    // A signal occurred.
  }
}

PmTimestamp pmTimerCallback(void* data)
{
  const uint64_t startTime = *(uint64_t*)data;
  return ntp_dur_to_ms(getNTPTime() - startTime);
}

PmDeviceID findDevice(const std::vector<std::tuple<PmDeviceID,std::string>>& devices, const std::string& arg_device)
{
    PmDeviceID device = -1;
    try {
      device = std::stoi(arg_device);
      auto it = std::find_if(devices.begin(), devices.end(), [&](const std::tuple<PmDeviceID,std::string>& x) {
        return std::get<0>(x) == device;
      });
      if (it == devices.end()) {
        std::stringstream s;
        s << "Invalid device ID " << arg_device;
        throw std::runtime_error(s.str());
      }
    } catch (std::invalid_argument) {
      auto it = std::find_if(devices.begin(), devices.end(), [&](const std::tuple<PmDeviceID,std::string>& x) {
        return std::get<1>(x) == arg_device;
      });
      if (it != devices.end()) {
        device = std::get<0>(*it);
      } else {
        std::stringstream s;
        s << "Device \"" << arg_device << "\" not found";
        throw std::runtime_error(s.str());
      }
    } catch (std::out_of_range) {
      std::stringstream s;
      s << "Invalid device ID " << arg_device;
      throw std::runtime_error(s.str());
    }
    return device;
}

void serverLoop(const std::string& arg_port, uint64_t startTime, std::shared_ptr<PortMidiStream> outputStream, boost::asio::io_service& io_service)
{
  using namespace boost::asio::ip;
  udp::resolver resolver(io_service);
  udp::resolver::query query(udp::v4(), "127.0.0.1", arg_port);
  udp::socket socket(io_service);
  socket.open(udp::v4());
  udp::endpoint endpoint = *resolver.resolve(query);

  socket.bind(endpoint);
  boost::array<char, 128> recv_buf;
  while (true) {
    udp::endpoint sender_endpoint;
    size_t len = socket.receive_from(
      boost::asio::buffer(recv_buf), sender_endpoint
    );
    handlePacket(startTime, startTime, OSCPP::Server::Packet(recv_buf.data(), len), outputStream);
  }
}

void midiLoop(const std::string& arg_port, uint64_t startTime, std::shared_ptr<PortMidiStream> inputStream, boost::asio::io_service& io_service, std::function<void(const MIDI::Message&)> callback)
{
    PmEvent eventBuffer[kEventBufferSize];

    while (true) {
        int numEvents = check(Pm_Read(inputStream.get(), eventBuffer, kEventBufferSize));
        for (int i=0; i < numEvents; i++) {
            const PmEvent& evt = eventBuffer[i];
            const int status = Pm_MessageStatus(evt.message);
            const int channel = status & 0x0F;
            const int command = status >> 4;
        
            MIDI::Message msg;
            msg.status = command;
            std::get<0>(msg.data) = Pm_MessageData1(evt.message);
            std::get<1>(msg.data) = Pm_MessageData2(evt.message);
            callback(msg);
        }
        usleep(500);
    }
}

void mainer(const std::vector<std::string>& args)
{
  boost::asio::io_service io_service;
  PortMidi pm(io_service);

  // Construct a signal set registered for process termination.
  // boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
  // Start an asynchronous wait for one of the signals to occur.
  // signals.async_wait(handler);

  if (args.size() != 3) {
    std::stringstream s;
    s << "Usage: frp_talk PORT INPUT_DEVICE OUTPUT_DEVICE" << std::endl;
    s << "\nInput devices:\n\n";
    for (auto x : pm.inputDevices()) {
      s << "(" << std::get<0>(x) << ") " << std::get<1>(x) << std::endl;
    }
    s << "\nOutput devices:\n\n";
    for (auto x : pm.outputDevices()) {
      s << "(" << std::get<0>(x) << ") " << std::get<1>(x) << std::endl;
    }
    throw std::runtime_error(s.str());
  }

  const std::string arg_port = args[0];
  const std::string arg_inputDevice = args[1];
  const std::string arg_outputDevice = args[2];

  const uint64_t startTime = getNTPTime();
  std::shared_ptr<PortMidiStream> inputStream;
  std::shared_ptr<PortMidiStream> outputStream;

  {
    PmDeviceID device = findDevice(pm.inputDevices(), arg_inputDevice);
    PortMidiStream* pmStream = nullptr;
    const PmError error = Pm_OpenInput(&pmStream,
                                       device,
                                       nullptr,
                                       kEventBufferSize,
                                       pmTimerCallback,
                                       const_cast<uint64_t*>(&startTime));
    if (error != pmNoError) {
      std::stringstream s;
      s << "Couldn't open MIDI device " << arg_inputDevice;
      throw std::runtime_error(s.str());
    }
    inputStream = std::shared_ptr<PortMidiStream>(pmStream, &Pm_Close);
  }

  {
    PmDeviceID device = findDevice(pm.outputDevices(), arg_outputDevice);
    PortMidiStream* pmStream = nullptr;
    const PmError error = Pm_OpenOutput(&pmStream,
                                        device,
                                        nullptr,
                                        kEventBufferSize,
                                        pmTimerCallback,
                                        const_cast<uint64_t*>(&startTime),
                                        kMIDILatency);
    if (error != pmNoError) {
      std::stringstream s;
      s << "Couldn't open MIDI device " << arg_outputDevice;
      throw std::runtime_error(s.str());
    }
    outputStream = std::shared_ptr<PortMidiStream>(pmStream, &Pm_Close);
  }

  // serverLoop(arg_port, startTime, outputStream, io_service);

  using namespace boost::asio::ip;
  udp::resolver resolver(io_service);
  udp::resolver::query query(udp::v4(), "127.0.0.1", arg_port);
  udp::endpoint endpoint = *resolver.resolve(query);

  udp::socket socket(io_service);
  socket.open(udp::v4());
  std::array<char, 8192> oscBuffer;

  // A sink (and event stream) of MIDI messages
  sodium::stream_sink<MIDI::Message> messageSink;

  // Perform initialization in a transaction
  sodium::transaction trans;

  // Separating I/O from logic:
  //
  // Construct the FRP network given its inputs.
  //
  // Output is a stream of synth messages and a stream of console messages.
  auto output = FRP::network(messageSink);

  // Register listeners for output events

  // Send synth events to synth
  std::get<0>(output).listen([&](const FRP::Synth& synth) {
    OSCPP::Client::Packet packet(oscBuffer.data(), oscBuffer.size());
    packet
      .openMessage("/play2", 8)
      .string("sound")
      .string(synth.sound().c_str())
      .string("amp")
      .float32(synth.amp())
      .string("speed")
      .float32(synth.speed())
      .string("pan")
      .float32(synth.pan())
      .closeMessage();
    socket.send_to(boost::asio::buffer(oscBuffer), endpoint);
  });

  // Send console events to console
  std::get<1>(output).listen([](const std::string& str) {
    std::cout << str << std::endl;
  });

  // Close transaction before entering main event loop
  trans.close();

  std::cout << "frp_talk: input device " << arg_inputDevice
            << ", output device " << arg_outputDevice
            << ", OSC port " << arg_port << std::endl;

  midiLoop(arg_port, startTime, inputStream, io_service, [&](const MIDI::Message& msg) {
    messageSink.send(msg);
  });
}

int main(int argc, const char** argv)
{
  const std::vector<std::string> args(argv+1, argv+argc);
  try {
    mainer(args);
    return 0;
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
}
