#include "frp.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

// Play sample when message arrives

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    stream<Synth> synths = input.map([](Message msg) -> Synth {
        return Synth("reverbkick");
    });

    stream<string> messages = input.map([](Message msg) {
        stringstream s;
        s << msg;
        return s.str();
    });

    return make_tuple(synths, messages);
}

// -> Play sample only on note-on
