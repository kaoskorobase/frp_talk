FRP_CPP := frp_1.cpp
FRP_CPPS = $(shell ls -1 frp_*.cpp)

HEADERS = frp.hpp midi.hpp
SOURCES = $(FRP_CPP) main.cpp

CXX_FLAGS = -std=c++11 -Wno-deprecated-declarations
INCLUDES = -I oscpp/include -I sodium-cxx 
LIBS = -lboost_system -lportmidi 

frp_talk: $(HEADERS) $(SOURCES) sodium-cxx/build/libsodium.a
	c++ $(CXX_FLAGS) $(DEFINES) $(INCLUDES) $(LIBS) -o $@ sodium-cxx/build/libsodium.a $(SOURCES)

sodium-cxx/build/libsodium.a:
	rm -rf sodium-cxx/build
	mkdir sodium-cxx/build
	( cd sodium-cxx/build && cmake .. )
	$(MAKE) -C sodium-cxx/build

.PHONY: check
check:
	for x in $(FRP_CPPS); do \
		rm -f frp_talk && make FRP_CPP=$$x ; \
	done

.PHONY: tags
tags:
	ctags -R --languages=c,c++ --c++-kinds=+p --fields=+iaS --extra=+q
