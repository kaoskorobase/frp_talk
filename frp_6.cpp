#include "frp.hpp"
#include "util.hpp"

#include <sstream>

using namespace FRP;
using namespace MIDI;
using namespace sodium;
using namespace std;

using boost::make_optional;
using boost::none;
using boost::optional;

// Use different samples

stream<Note> notes(stream<Message> messages)
{
    return filter_optional(messages.map([](Message msg) -> optional<Note> {
        switch (msg.status) {
        case kNoteOn:
          return Note(Note::On, get<0>(msg.data), get<1>(msg.data) / 127.0);
        case kNoteOff:
          return Note(Note::Off, get<0>(msg.data), get<1>(msg.data) / 127.0);
        default:
          return none;
        }
    }));
}

optional<int> samplePad(const Note& note)
{
    return note.note >= 44 && note.note <= 51
        ? make_optional(note.note - 44)
        : none;
}

tuple<stream<Synth>, stream<string>> FRP::network(stream<Message> input)
{
    static const auto sampleMap = FRP::readFileLines("samples1.txt");

    stream<Synth> synths = filter_optional(
        notes(input)
        .filter([](Note note) { return note.state == Note::On; })
        .map([&](Note note) {
            const auto pad = samplePad(note);
            return pad
                ? make_optional(Synth(sampleMap.at(*pad), note.velocity))
                : none;
        })
    );

    stream<string> messages = input.map([](Message msg) {
        stringstream s;
        s << msg;
        return s.str();
    });

    return make_tuple(synths, messages);
}

// -> Alternative sample map
